# Курс "Алгоритмы, программирование и структуры данных"

### Задачи занятия № 06: Основные структуры данных. Стек, очередь, дек

- [X] Реализовать класс Stack на массиве(списке) (файл - stack.py)
- [X] Выполнить домашнее задание. Реализовать класс StackMax (stack_max.py)
- [X] Выполнить домашнее задание. Реализовать проверку правильной скобочной последовательности (bracket_seq.py)
- [X] Реализовать очередь на кольцевом буффере (queue_circle_buffer.py)
- [X] Выполнить домашнее задание. Реализовать очередь на связанных списка (queue_linked_list.py)

Теория: 
1. Структура данных стек: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=402%3A3
2. Структуры данных: очередь и дек: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=402%3A48
3. Структура данных очередь. Реализация: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=403%3A78
