# Курс "Алгоритмы, программирование и структуры данных"

### Задачи занятия № 07: Стек вызовов, рекурсия

- [X] Реализовать функцию матрешка (файл - matryoshka.py);
- [X] Реализовать функцию для вычисления двоичной записи любого числа (binary_number.py);
- [X] Реализовать функцию бинарного поиска (binary_search.py);
- [X] Домашнее задание. Два велосипеда (bikes.py)
- [X] Домашнее задание. Генератор скобок (generator.py)
- [X] Домашнее задание. Комбинации (combination.py)

Теория: 
1. Примеры задач на рекурсию: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=464%3A3
2. Рекурсивный и базовый случай: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=464%3A29
3. Реализация бинарного поиска с помощью рекурсии: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=464%3A31
4. Рекурсивный перебор вариантов: https://www.figma.com/file/83PQbJXKbUWVNEFR1lMlHq/theory?node-id=464%3A33
